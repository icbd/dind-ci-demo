package main

import (
	"fmt"
	"runtime"
)

func main() {
	fmt.Printf("Hello World. \nGO version: %s\n", runtime.Version())
}
