FROM golang:1.15-alpine as builder
WORKDIR /usr/build
ADD main.go .
RUN go build -o hello_world .

FROM alpine:latest
WORKDIR /usr/src

COPY --from=builder /usr/build/hello_world .
CMD ["/usr/src/hello_world"]
